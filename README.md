# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado?(2016)
```plantuml
@startmindmap
+[#Tan] Programando en ordenadores
++[#PaleGreen] Antes
+++[#PowderBlue] Características del ordenador
++++[#YellowGreen] - Recurso limitados\n- La arquitectura era diferente para cada ordenador\n- Poco potentes
+++[#PowderBlue] Lenguaje de  bajo nivel
++++[#YellowGreen] Luenguaje ensamblador
+++++[#Khaki] Ventajas
++++++[#LightGray] - Es muy eficiente al momento de ejecutar.\n- Aprovecha de mejor manera los recursos
+++++[#Khaki] Desventajas
++++++[#LightGray] - Era nesesario conocer la arquitectura\nde cada ordenador\n- Es complicado de entender
+++[#PowderBlue] Forma de programar
++++[#YellowGreen] La forma de programar era más complicada debido a que \nse tenía que tener conocimiento de la arquitectura de cada ordenador \ny ademas el lenguaje era muy difícil de comprender, era muy complicado \nrealizar un software complejo.
++[#PaleGreen] Después
+++[#PowderBlue] Características del ordenador
++++[#YellowGreen] - Suficientes recursos\n- Mas potentes\n- Mas Veloces
+++[#PowderBlue] Lenguajes de alto nivel
++++[#YellowGreen] - JAVA\n- C\n- C++
+++++[#Khaki] Ventajas
++++++[#LightGray] - Son lenguajes muy faciles de entender\n- No es nesesario conocer la arquitectura del ordenador
+++++[#Khaki] Desventajas
++++++[#LightGray] - Su ejecución es muy lenta\n- Consume muchos recursos
+++[#PowderBlue] Forma de programar
++++[#YellowGreen] La forma de programar es mas sencilla debido a que los \nlenguajes actuales son mas fáciles de entender, son portables y \npermiten desarrollar software complejo de una manera más fácil.
@endmindmap
```
# Hª de los algoritmos y de los lenguajes de programación (2010)
```plantuml
@startmindmap
+[#Violet] Hª de los algoritmos y de los lenguajes de programación
++[#LightPink] Algoritmos
+++[#Khaki] Son una lista de pasos bien definidos que \npermiten dar solución a un problema
++++[#PaleGreen] Historia
+++++[#PowderBlue] Su historia comienza casi igual que la de la humanidad, en la antigua \nMesopotamia se empleaban algoritmos para describir ciertos cálculos, \nen el siglo XVII aparecieron las primeras calculadoras mecánicas que \nimplementaban un algoritmo, y en el siglo XIX aparecieron las primeras \nmáquinas programables. 
+++[#Khaki] Caracteristicas
++++[#PaleGreen] - Es una serie de instrucciones\n- Es finito\n- Es claro\n- Resuleve una problematica
+++++[#PowderBlue] Su importancia radica en mostrar la manera de \nllevar a cabo procesos y resolver problemas
++[#LightPink] Lenguaje de programación
+++[#Khaki] Es un lenguaje formal o artificial con reglas \ngramaticales bien definidas que le proporciona la capacidad \nde programar una serie de instrucciones
++++[#PaleGreen] Tipos de paradigmas
+++++[#PowderBlue] - Paradigma imperativo\n- Paradigma funcional\n- Paradigma orientado a objetos\n- Paradigma lógico 
+++[#Khaki] Lenguaje mas utilizado
++++[#PaleGreen] Empresas
+++++[#PowderBlue] - JAVA\n- C++\n- C
++++[#PaleGreen] Comunidad científica
+++++[#PowderBlue] - Fortran\n- Lisp
@endmindmap
```
# Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación(2005)
```plantuml
@startmindmap
+[#PowderBlue] Tendencias actuales en los lenguajes \nde Programación y Sistemas Informáticos
++[#LightGray] Paradigmas de programación
+++[#Plum] Es la manera en la que podemos concebir la programación de \nuna manera determinada para dar una solución concreta
++++[#Khaki] Paradigma estructurado
+++++[#Salmon] Ofrece un diseño de software modular, cuando se presenta un problema, \neste puede descomponerse en problemas más pequeños hasta llegar \na un problema de resolución más sencilla
++++[#Khaki] Paradigma funcional
+++++[#Salmon] Se basa en usar el lenguaje de las matemáticas para buscar una solución \nal problema, se  hace uso de la recursividad
++++[#Khaki] Paradigma lógico
+++++[#Salmon] Se basa en expresiones lógicas para buscar una solución al problema,no \nexisten operaciones aritméticas
++++[#Khaki] Paradigma orientado a objetos
+++++[#Salmon] Ofrece un nuevo diseño de software en cual permite crear los \nelementos que van a intervenir en la solución
++++[#Khaki] Paradigma basado \nen los componentes
+++++[#Salmon] Ofrece la posibilidad de ver los componentes como objetos \nresultando en un mayor nivel de abstracción
++++[#Khaki] Paradigma orientado \na aspectos
+++++[#Salmon] Los aspectos pueden programarse separada mente y \nsolo al final se pueden integrar
++++[#Khaki] Paradigma orientado \na gente software
+++++[#Salmon] Este paradigma es enfocado en el campo de \nla inteligencia artificial
+++[#Plum] Tendencia actual
++++[#Khaki] La tendencia se encuentra muy marcada rumbo a la abstracción, a la búsqueda \nde paradigmas más abstractos que nos alejen de la máquina y se acerquen más \na nuestra forma nativa de pensar
@endmindmap
```
